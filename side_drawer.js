function openSideDrawer() {
    document.getElementById("side-drawer").style.right = "0";
    document.getElementById("side-drawer-void").classList.add("d-block");
    document.getElementById("side-drawer-void").classList.remove("d-none");
}

function closeSideDrawer() {
    document.getElementById("side-drawer").style.right = "-336px";
    document.getElementById("side-drawer-void").classList.add("d-none");
    document.getElementById("side-drawer-void").classList.remove("d-block");
}

function dialogSideDrawer() {
    document.getElementById("dialog-side-drawer").style.right = "15px";
    document.getElementById("card-dialogos-lista").classList.add("evidencia-card");
    document.getElementById("dialog-side-drawer-create").style.right = "-48%";
}

function dialogCloseSideDrawer() {
    document.getElementById("dialog-side-drawer").style.right = "-48%";
    document.getElementById("card-dialogos-lista").classList.remove("evidencia-card");
}

function createDialogSideDrawer() {
    document.getElementById("dialog-side-drawer-create").style.right = "15px";
    document.getElementById("dialog-side-drawer").style.right = "-48%";
    document.getElementById("card-dialogos-lista").classList.remove("evidencia-card");
}

function closeCreateDialogSideDrawer() {
    document.getElementById("dialog-side-drawer-create").style.right = "-48%";
}

function delete_alert(){
    alert('ATENÇÃO! Você está prestes a excluir esse registro!');
}